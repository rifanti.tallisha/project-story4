from django.test import TestCase, Client
from django.urls import reverse, resolve
from .views import homest7

# Create your tests here.
class teststory7(TestCase):
    def test_url(self):
        response = Client().get('/story7/')
        self.assertEqual(response.status_code, 200)

    def test_template(self):
        response = Client().get('/story7/')
        self.assertTemplateUsed(response, 'buatstory7.html')