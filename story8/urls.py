from django.urls import path, include
from . import views

app_name = 'story8'

urlpatterns = [
    path('', views.search, name = 'search'),
    path('data/', views.caridata),
]