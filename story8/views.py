from django.shortcuts import render
from django.http import HttpResponseRedirect, JsonResponse
import requests
import json


# Create your views here.
def search(request):
    return render(request, 'buatstory8.html')

def caridata(request):
    param = request.GET['q']
    url_dest = 'https://www.googleapis.com/books/v1/volumes?q=' + param
    res = request.get(url_dest)
    datanya = json.loads(res.content)
    return JsonResponse(datanya, safe=False)