from django.test import TestCase, Client
from django.urls import reverse, resolve
from .views import search

# Create your tests here.
class TestStort8(TestCase):
    def test_url(self):
        response = Client().get('/story8/')
        self.assertEqual(response.status_code, 200)

    def test_template(self):
        response = Client().get('/story8/')
        self.assertTemplateUsed(response, 'buatstory8.html')