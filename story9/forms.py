from django import forms

class buatlogin(forms.Form):
    username = forms.CharField(widget=forms.TextInput(attrs={
        'class' : 'form-control',
        'type' : 'text',
        'placeholder' : "Username",
        'required' : True,
    }))
    password = forms.CharField(widget=forms.PasswordInput(attrs={
        'class' : 'form-control',
        'type' : 'password',
        'placeholder' : "Password",
        'required' : True,
    }))

class buatbaru(forms.Form):
    username = forms.CharField(widget=forms.TextInput(attrs={
        'class' : 'form-control',
        'type' : 'text',
        'placeholder' : "Username",
        'required' : True,
    }))
    email = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control', 
        'type' : 'text', 
        'placeholder' : 'Email' , 
        'required' : True,
    }))
    password = forms.CharField(widget=forms.PasswordInput(attrs={
        'class' : 'form-control',
        'type' : 'password',
        'placeholder' : "Password",
        'required' : True,
    }))