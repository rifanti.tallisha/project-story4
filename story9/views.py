from django.shortcuts import render, redirect
from .forms import buatlogin, buatbaru
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.http import HttpResponseRedirect

# Create your views here.
def home(request):
    return render(request, 'databuku.html')

def masuk(request):
    if request.method == "POST":
        form = buatlogin(request.POST)
        if form.is_valid():
            usrnm = form.cleaned_data["username"]
            psswrd = form.cleaned_data["password"]
            check = authenticate(request, username=usrnm, password=psswrd)
            if check is not None:
                login(request, check)
                return redirect('/story9/')
            else:
                return redirect('/story9/daftar/')
    else:
        form = buatlogin()
        response = {"form" : form}
        return render(request, "buatstory9.html", response)

def baru(request):
    if request.method == "POST":
        form = buatbaru(request.POST)
        if form.is_valid():
            usrnm = form.cleaned_data["username"]
            emailnya = form.cleaned_data["email"]
            psswrd = form.cleaned_data["password"]
            try:
                user = User.objects.get(username = usrnm)
                return redirect("/story9/daftar/")
            except User.DoesNotExist:
                user = User.objects.create_user(usrnm, emailnya, psswrd)
                return redirect("/story9/")
    else:
        form = buatbaru()
        response = {"form":form}
        return render(request, "buatbaru.html", response)

def keluar(request):
    logout(request)
    return redirect('/story9')

def caridata(request):
    param = request.GET['q']
    url_dest = 'https://www.googleapis.com/books/v1/volumes?q=' + param
    res = request.get(url_dest)
    datanya = json.loads(res.content)
    return JsonResponse(datanya, safe=False)