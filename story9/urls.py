from django.urls import path, include
from . import views

app_name = 'story9'

urlpatterns = [
    path('', views.home, name = 'home'),
    path('login/', views.masuk, name = 'login'),
    path('daftar/', views.baru, name='daftar'),
    path('logout/', views.keluar, name='logout'),
    path('data/', views.caridata),
]