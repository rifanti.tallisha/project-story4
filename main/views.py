from django.shortcuts import render, get_object_or_404,redirect
from django.http import HttpResponseRedirect
from .forms import Input_Form
from .models import matkul

def home(request):
    return render(request, 'main/FrontPage.html')

def profile(request):
    return render(request, 'main/MainPage.html')

def album(request):
    return render(request, 'main/albumpage.html')

def matkulpage(request):
    infomatkul = matkul.objects.all()
    print (infomatkul)
    response = {'matkul' : infomatkul}
    return render(request, 'main/MatkulPage.html', response)

def index(request):
    if (request.method == 'POST'):
        form = Input_Form(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('matkul')
    else:
        form = Input_Form()
    response = {'input_form' : Input_Form}       
    return render(request, 'main/index.html',response)

def deletematkul(request, id):
    nama = get_object_or_404(matkul, id=id)
    nama.delete()
    return redirect('main:matkulpage')

