from django.db import models

# Create your models here.
class matkul(models.Model):
    NamaMatkul = models.CharField(max_length=200)
    dosen = models.CharField(max_length=200)
    sks = models.CharField(max_length=200)
    deskripsi = models.CharField(max_length=200)
    tahun = models.CharField(max_length=200)
    ruangkelas = models.CharField(max_length=200)
    
    def __str__(self):
        return self.NamaMatkul
