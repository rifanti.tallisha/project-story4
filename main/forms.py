from django import forms
from .models import matkul

class Input_Form(forms.ModelForm):
    class Meta:
        model = matkul
        fields = '__all__'
        widgets = {
        'NamaMatkul' : forms.TextInput(attrs={'class': 'form-control'}),
        'dosen' : forms.TextInput(attrs={'class': 'form-control'}),
        'sks' : forms.TextInput(attrs={'class': 'form-control'}),
        'deskripsi' : forms.TextInput(attrs={'class': 'form-control'}),
        'tahun' : forms.TextInput(attrs={'class': 'form-control'}),
        'ruangkelas' : forms.TextInput(attrs={'class': 'form-control'}),
    }
    error_messages = { 'required' : 'Please Type' }

    input_attrs = {
        'type' : 'text',
    }
