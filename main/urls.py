from django.urls import path, include
from . import views

app_name = 'main'

urlpatterns = [
    path('', views.home, name='home'),
    path('mainprofile', views.profile, name='profile'),
    path('album', views.album, name='album'),
    path('form', views.index, name='index'),
    path('matkul', views.matkulpage, name='matkulpage'),
    path('hapus/<int:id>', views.deletematkul, name='deletematkul')
]
